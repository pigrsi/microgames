extends Sprite

var speed = 0

func _process(delta):
	speed += 0.0000005
	modulate.a = min(1, modulate.a + speed)
	if modulate.a == 1: get_tree().quit()
