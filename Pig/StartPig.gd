extends AnimatedSprite

var fast = false

func _process(delta):
	position.x += 1
	if fast: position.x += 20

func upgrade():
	fast = true

func _on_Area2D_input_event(viewport, event, shape_idx):
	if fast and event is InputEventMouseButton:
		get_tree().change_scene("res://Bedroom.tscn")
