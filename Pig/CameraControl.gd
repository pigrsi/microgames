extends Camera2D

var zoom_t = 0
	
func zoom_to(distance, seconds):
	$ZoomTween.interpolate_property(self, "zoom", zoom, 
	Vector2(distance, distance), seconds, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	$ZoomTween.start()
	
func look(new_position, seconds):
	$MoveTween.interpolate_property(self, "position", position,
	new_position, seconds, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	$MoveTween.start()
	
func stop():
	$ZoomTween.remove_all()
	$MoveTween.remove_all()

func _process(delta):
	pass
