extends Camera

func _ready():
	var initial_pos = transform.origin
	var pig = get_tree().get_root().find_node("Pig", true, false)
	transform.origin = Vector3(pig.transform.origin)
	transform.origin.z += 0.055
	
	$Tween.interpolate_property(self, "transform:origin", transform.origin, 
	initial_pos, 5, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	$Tween.start()

