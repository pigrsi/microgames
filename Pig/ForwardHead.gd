extends Area

onready var target_z = transform.origin.z
export var wait_time = 12.0
export var instant_noise = false
export var back_by = 0.25
export var speed = 0.003
export var scene_changer = false
var timer_started = false

func _ready():
	translate(Vector3(0, 0, -back_by))
	$Timer.wait_time = wait_time
	
func _process(delta):
	if not timer_started or not $Timer.is_stopped(): return
	
	if transform.origin.z < target_z:
		translate(Vector3(0, 0, speed))
		if transform.origin.z >= target_z:
			$NoiseTimer.wait_time = randf() * 8.0
			if instant_noise: 
				$NoiseTimer.wait_time = 0.1
				$Noise.unit_db += 5
			$NoiseTimer.start()
	elif scene_changer:
		get_tree().change_scene("res://WakeUp.tscn")

func go():
	$Timer.start()
	timer_started = true


func _on_NoiseTimer_timeout():
	$Noise.play()
	$NoiseTimer.wait_time = randf() * 8.0
