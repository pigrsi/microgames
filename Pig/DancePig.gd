extends AnimatedSprite3D

var dance = -1
var floor_height
var velocity = Vector3()
var scared = false

func _ready():
	floor_height = transform.origin.y

func _process(delta):
	if scared: 
		offset = Vector2(-7 + randf() * 12.6, 0)
		return
		
	if dance == -1: return
		
	transform.origin.y += velocity.y * delta
	
	match dance:
		0:
			if transform.origin.y <= floor_height:
				dance = -1
				velocity.y = 0
				transform.origin.y = floor_height
			else:
				velocity.y -= 12 * delta
		_:
			if not $Tween.is_active():
				dance = -1

func dance(num):
	if dance != -1: return
	dance = num
	
	get_tree().call_group("DanceLabel", "on_dance", dance)
	
	match dance:
		0:
			velocity.y = 3
			$Tween.interpolate_property(self, "rotation_degrees", Vector3(), Vector3(0, 0, 360), 0.5,
			Tween.TRANS_LINEAR, Tween.EASE_IN)
		1:
			$Tween.interpolate_property(self, "scale", null, Vector3(0.15, 0.05, 1), 0.25,
			Tween.TRANS_LINEAR, Tween.EASE_IN)
			$Tween.interpolate_property(self, "scale", Vector3(0.15, 0.05, 1), Vector3(0.15, 0.15, 1), 0.25,
			Tween.TRANS_LINEAR, Tween.EASE_IN, 0.25)
		2:
			$Tween.interpolate_property(self, "rotation_degrees", Vector3(), Vector3(0, 360, 0), 0.5,
			Tween.TRANS_LINEAR, Tween.EASE_IN)
		3:
			$Tween.interpolate_property(self, "rotation_degrees", Vector3(), Vector3(0, -360, 0), 0.5,
			Tween.TRANS_LINEAR, Tween.EASE_IN)
			
	$Tween.start()

func _input(event):
	if event is InputEventKey:
		if event.is_action_pressed("up"):
			dance(0)
		elif event.is_action_pressed("down"):
			dance(1)
		elif event.is_action_pressed("left"):
			dance(2)
		elif event.is_action_pressed("right"):
			dance(3)
			
func stop():
	scared = true
	playing = false
	frame = 2
