extends Sprite

var state = "idle"

var text = ["i could help again...click the pig next time",
"you needa click the pig!", "click the pig you......",
"this aint my day job", "this sucks", "runnin after pigs", "pig click", "youre bad", "your bad click the pig youre bad click the pig!"]
var text_index = 0

func _process(delta):
	if state == "idle":
		if get_tree().get_root().find_node("Pig", true, false).position.x > 3500:
			state = "query"
			$Bubble.visible = true
	elif state == "chase":
		position.x += (7 + text_index * 3)
		if position.x > 7200:
			state = "drag"
			position = Vector2(1200, 300)
			flip_h = false
			if text_index == 3:
				$Particles2D.visible = true
	elif state == "drag":
		if position.x > 450: 
			position.x -= 2
			get_tree().get_root().find_node("Pig", true, false).position = position + Vector2(50, 0)
		else:
			$Bubble/Label.text = text[text_index]
			text_index = min(text.size() - 1, text_index + 1)
			state = "idle"


func _on_Area2D_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if state == "query":
			state = "chase"
			$Bubble.visible = false
			flip_h = true
