extends Sprite

var fly = false

func _process(delta):
	if not fly: return
	position += Vector2(-0.2, -0.35)
	scale -= Vector2(0.0035, 0.0035)
	rotation_degrees -= 0.8
	if scale.x < 0: queue_free()
