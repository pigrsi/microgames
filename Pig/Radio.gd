extends Sprite

var songs = [load("res://music/good_morning.ogg"),
 load("res://music/milky_way.ogg"),
 load("res://music/buff_correll_queen.ogg"),
 load("res://music/jack_peril.ogg"),
 load("res://music/shreik1.ogg"),
 load("res://music/spaghetti.ogg"),
 load("res://music/toona.ogg"),
 load("res://music/wenttolidl.ogg")]
var clickSound = load("res://sfx/click.wav")
var current = 0
var clicking = false

func _ready():
	play(0)
	
func _process(delta):
	if $RadioPlayer.get_playback_position() >= $RadioPlayer.stream.get_length():
		if clicking:
			next()
			clicking = false
		else:
			click()
	
	
func click():
	$RadioPlayer.stream = clickSound
	$RadioPlayer.play()
	clicking = true
	
func next():
	current += 1
	current = current % songs.size()
	play(current)
	
func play(num):
	$RadioPlayer.stream = songs[num]
	
	var from = 0
	match num:
		1:
			from = 3
		2:
			from = 71
			
	$RadioPlayer.play(from)
	


func _on_Area2D_input_event(viewport, event, shape_idx):
	if clicking: return
	if event is InputEventMouseButton and event.pressed:
		click()


func _on_Area2D_mouse_entered():
	scale -= Vector2(0.05, 0.05)


func _on_Area2D_mouse_exited():
	scale += Vector2(0.05, 0.05)
