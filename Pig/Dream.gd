extends Node

var skipping = false

func _ready():
	$Heads.visible = true
	$MajorHeads.visible = true


func _on_BadTimer_timeout():
	get_tree().call_group("Head", "go")
	

func _on_PreTimer_timeout():
	get_tree().call_group("PartyPig", "stop")
	get_tree().call_group("DanceLabel", "go_crazy")
	skipping = true
	$Timers/SkipTimer.start()

func seekback():
	$AudioStreamPlayer.seek($AudioStreamPlayer.get_playback_position() - $Timers/SkipTimer.wait_time)

func _on_SkipTimer_timeout():
	seekback()
