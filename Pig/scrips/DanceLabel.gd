extends Label

export var type = 0
const texts = ["Nice!", "Cool!", "Stylish!", "Sick!", "Hot!", "Sweet!", "Damn!", "Wow!", "Hey!", "Man!", "Woah!", "And how!", "OMG!", "Oh! Oh!", "Moves!", "Nice moves!", "Bam!", "OHMG!", "Good!", "Mad!", "MAD!", "Dance!", "Cray!", "Spin!", "Wham!", "OK!", "Decent!", "Mmm!", "Bad", "Um no", "Ugh", "Woooo!", "HEY!"]
var crazy = false

func _process(delta):
	if not crazy: return
	text = texts[randi() % texts.size()]

	
func on_dance(num):
	if num != type: return
	$Timer.start()
	$Sound.play()
	
	text = texts[randi() % texts.size()]


func _on_Timer_timeout():
	text = ""


func go_crazy():
	$SpazTimer.start()


func _on_SpazTimer_timeout():
	crazy = true
	add_color_override("font_color", Color(1, 0, 0))
