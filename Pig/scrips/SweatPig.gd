extends AnimatedSprite

var progress = 0
var node = 0
var start_pos
var pig_path
var go = false

func _ready():
	start_pos = position
	pig_path = get_tree().get_root().find_node("PigPath", true, false)

func _process(delta):
	offset = Vector2(-5 + randf() * 10, -5 + randf() * 10)
	
	if not go: return
	
	position = start_pos + pig_path.curve.interpolate(node, progress)
	progress = min(1, progress + 0.075)
	
	if progress == 1:
		if node < pig_path.curve.get_point_count():
			node += 1
			progress = 0
		else:
			get_tree().change_scene("res://Leave.tscn")



func _on_Area2D_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		go = true
		play()


func _on_Area2D_area_entered(area):
	if $bubble: $bubble.queue_free()
