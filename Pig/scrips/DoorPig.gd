extends AnimatedSprite

func _ready():
	scale
	$Tween.interpolate_property(self, "position", null, $Target.global_position, 5, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Tween.interpolate_property(self, "scale", null, Vector2(0.25, 0.25), 5, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Tween.start()


func _on_Area2D_area_entered(area):
	get_tree().get_root().find_node("door", true, false).fly = true
