extends Sprite

var vibrating = false

func _process(delta):
	if vibrating:
		offset = Vector2(-20 + randf() * 40, -20 + randf() * 40)
	else:
		offset = Vector2()

func _on_Area2D_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		get_tree().call_group("Cutscene", "play", "into_dream")

func _on_VibrateTimer_timeout():
	vibrating = !vibrating

func _on_FlipTimer_timeout():
	flip_h = !flip_h
