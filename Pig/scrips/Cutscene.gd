extends Node

onready var cutscenes = {
	"intro": [zoom(1.25, 7), look("Background", 10)],
	"into_dream": [zoom(0.01, 5), look("SleepingPig", 3), wait(5), scene("Dream")]
}
onready var camera = get_tree().get_root().find_node("SuperCam", true, false)
var stage = 0
export var playing = ""

func zoom(to, time):
	return { "action": "zoom", "args": [to, time] }
	
func look(at, time):
	return { "action": "look", "args": [get_tree().get_root().find_node(at, true, false).position, time] }
	
func wait(time):
	return { "action": "wait", "args": [time] }
	
func scene(new_scene):
	return { "action": "scene", "args": [new_scene] }
	
func play(name, force=false):
	if playing != name:
		camera.stop()
		playing = name
		stage = 0
	
func _process(delta):
	if not playing: return
	if not $WaitTimer.is_stopped(): return
	
	while stage < cutscenes[playing].size():
		var bit = cutscenes[playing][stage]
		stage += 1
		if bit.action == "zoom":
			camera.zoom_to(bit.args[0], bit.args[1])
		elif bit.action == "look":
			camera.look(bit.args[0], bit.args[1])
		elif bit.action == "wait":
			$WaitTimer.wait_time = bit.args[0]
			$WaitTimer.start()
			break
		elif bit.action == "scene":
			get_tree().change_scene("res://" + bit.args[0] + ".tscn")
