extends Sprite3D

var start_y
var velocity = Vector3(2, 10, 0)
export var spin = false
var stop = false

func _ready():
	start_y = transform.origin.y

func _process(delta):
	if stop: return
	
	if spin:
		rotate_y(0.1)
	else:
		velocity.y -= 1
		
		if transform.origin.y < start_y:
			velocity.y = 10
			velocity.x *= -1
			transform.origin.y = start_y
		
		translate(velocity * delta)

func stop():
	stop = true
