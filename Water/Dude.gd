extends KinematicBody2D

func _process(delta):
	if Input.is_action_pressed("rotate_left"):
		rotation_degrees -= 1
	elif Input.is_action_pressed("rotate_right"):
		rotation_degrees += 1

	var speed = Vector2()
	if Input.is_action_pressed("go"):
		speed = Vector2(2, 0).rotated(rotation - PI/2)
	elif Input.is_action_pressed("back"):
		speed = Vector2(2, 0).rotated(rotation + PI/2)
		
	if Input.is_action_pressed("boost"):
		speed = speed * 2
		
	translate(speed)


func _on_Area2D_area_entered(area):
	pass # Replace with function body.

func win():
	$Sprite.texture = load("res://gfx/dude2.png")
	
func unwin():
	$Sprite.texture = load("res://gfx/dude.png")
