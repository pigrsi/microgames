extends Label

var drops = 0
var num_drops = 0
var won = false

func update():
	text = "in the bowl: " + str(drops) + "/" + str(num_drops)
	if drops >= 100 and drops == num_drops:
		get_tree().get_root().find_node("Dude", true, false).win()
		won = true
	elif won and drops < num_drops:
		get_tree().get_root().find_node("Dude", true, false).unwin()
	
func new_drop():
	num_drops += 1
	update()

func _on_Area2D_body_entered(body):
	drops += 1
	update()

func _on_Area2D_body_exited(body):
	drops -= 1
	update()


func _on_DeathZone_body_entered(body):
	num_drops -= 1
	update()
