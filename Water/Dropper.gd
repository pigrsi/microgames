extends Node2D

export var numberOfDrops = 1

const DropResource = preload("res://Drop.tscn")

func _ready():
	$Sprite.visible = false
		

func _on_Timer_timeout():
	var inst = DropResource.instance()
	get_tree().get_root().add_child(inst)
	inst.position = global_position
	numberOfDrops -= 1
	get_tree().call_group("HUD", "new_drop")
	if numberOfDrops > 0: 
		$Timer.start()
	else:
		queue_free()
