extends Area2D

var target

func _ready():
	$Label.text = ""

func _process(delta):
	if not target: return
	position = target.position - Vector2(117, 190)

func _on_Mush_area_entered(area):
	if (area.get_collision_layer_bit(1)):
		target = area
		show_text("Gobble gobble...hi Alex...you wouldn't be willing to help me hunt down my 5-snax-a-day, would you?")
	if (area.get_collision_layer_bit(3)):
		area.queue_free()
		show_text("gobble gobble gooble gubble gubble gubble")
	

func show_text(text):
	$Label.text = text
	$TextTimer.start()

func _on_TextTimer_timeout():
	$Label.text = ""
