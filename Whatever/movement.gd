extends Area2D


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	
func _process(delta):
	if Input.is_action_pressed("left"):
		translate(Vector2(-7, 0))
	elif Input.is_action_pressed("right"):
		translate(Vector2(7, 0))
		
	if Input.is_action_pressed("up"):
		translate(Vector2(0, -7))
	elif Input.is_action_pressed("down"):
		translate(Vector2(0, 7))
