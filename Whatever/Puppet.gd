extends Area2D

var totation_speed = 0
var inside = false
var FruitResource = preload("res://Fruit.tscn")
	
func _process(d):
	if (inside): 
		totation_speed += 0.12
	elif totation_speed > 0:
		totation_speed -= 0.12
		
	rotation_degrees += totation_speed
	
	if totation_speed > 50:
		become_fruit()

func _on_Puppet_area_entered(area):
	inside = true

func _on_Puppet_area_exited(area):
	inside = false

func become_fruit():
	var become = FruitResource.instance()
	become.position = position
	get_tree().root.add_child(become)
	queue_free()
