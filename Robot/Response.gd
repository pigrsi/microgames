extends NinePatchRect

const appearDuration = 0.5
var sitPoint
var spitLabel = preload("res://SpitLabel.tscn")

signal selected(nodeName)

func _init():
	rect_scale = Vector2()
	
func _ready():
	sitPoint = rect_position

func show_box(text):
	$Label.text = text
	$ShowHideTween.stop_all()
	$ShowHideTween.interpolate_property(self, "rect_scale", Vector2(), Vector2(1, 1), appearDuration, Tween.TRANS_QUAD, Tween.EASE_IN)
	$ShowHideTween.interpolate_property(self, "rect_position", Vector2(0, 100), sitPoint, appearDuration, Tween.TRANS_QUAD, Tween.EASE_IN)
	$ShowHideTween.start()
	
func hide_box():
	$ShowHideTween.stop_all()
	rect_scale = Vector2()
	
func spit_label():
	var inst = spitLabel.instance()
	inst.text = $Label.text
	get_tree().get_root().add_child(inst)
	inst.rect_position = rect_global_position
	
func _gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			emit_signal("selected", name)
