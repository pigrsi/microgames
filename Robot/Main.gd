extends Node2D

func _ready():
	$FileDialog.show()

func _on_FileDialog_file_selected(path):
	var dialog = $FileDialog

	var file = File.new()
	file.open(dialog.get_current_path(), File.READ)
	var content = JSON.parse(file.get_as_text())
	file.close()

	var tunkerBox = load("res://TunkerBox.tscn").instance()
	add_child(tunkerBox)
	tunkerBox.feed_data(content)
