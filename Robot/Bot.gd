extends Sprite

var emotion
export var talking = false

func _ready():
	$Responses.show()
	select_emotion("neutral")
	
func _process(delta):
	if talking: 
		$Mouth.animation = emotion.talk
	else:
		$Mouth.animation = emotion.mouth

func select_emotion(name):
	emotion = $System.get_emotion(name)
	$Eyes.animation = emotion.eyes
	$Mouth.animation = emotion.mouth
	$Eyes.frame = 0
	$Mouth.frame = 0

func say(text, delay=0):
	$SpeechContainer.say(text, delay)

func stop_talking(delay=0):
	$SpeechContainer.hide_box(delay)
