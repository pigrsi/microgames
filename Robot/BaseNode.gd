extends Node2D
class_name BaseNode

func get_data():
	return {
		"name": name,
		"x": position.x,
		"y": position.y,
		"outputs": get_outputs()
	}

func get_outputs():
	var outputs = []
	for output in $Outputs.get_children():
		var input = output.get_connected_node()
		if input: 
			outputs.append(input.get_parent().name)
		else:
			outputs.append(null)
	return outputs
