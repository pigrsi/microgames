extends Node

var emotions

func _init():
	load_json()

func load_json():
	var file = File.new()
	file.open("res://emotions.json", File.READ)
	var content = file.get_as_text()
	emotions = JSON.parse(content).result
	file.close()

func get_emotion(name):
	return emotions[name]
	
func get_list_of_emotions():
	return emotions.keys()
