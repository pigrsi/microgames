extends Camera2D

var dragging = false

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if event.pressed:
				dragging = true
			else:
				dragging = false
	elif dragging and event is InputEventMouseMotion:
		position -= event.relative * 3
	elif Input.is_action_just_pressed("ui_cancel"): 
		position = Vector2()
