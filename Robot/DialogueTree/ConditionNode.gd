extends BaseNode

func set_values(text):
	$PanelContainer/HBoxContainer/LineEdit.text = text

func get_data():
	var data = .get_data()
	data["type"] = "ConditionNode"
	data["text"] = $PanelContainer/HBoxContainer/LineEdit.text
	
	return data


func _on_condition_changed(_new_text):
	var text = $PanelContainer/HBoxContainer/LineEdit.text
	if !text: return
	
	for i in range(0, $Outputs.get_child_count() - 1):
		$Outputs.get_child(i).set_popup_text("")
		
	
	for node in get_parent().get_children():
		if node.has_method("get_condition_id") and text == node.get_condition_id():
			for i in range(0, node.get_node("Outputs").get_child_count()):
				var connected = node.get_node("Outputs").get_child(i).get_connected_node()
				if connected:
					$Outputs.get_child(i).set_popup_text(connected.get_parent().get_data()["text"])


func _on_Timer_timeout():
	_on_condition_changed(null)
