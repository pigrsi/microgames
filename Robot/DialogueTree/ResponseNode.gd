extends BaseNode

func set_values(text):
	$PanelContainer/Response.text = text

func get_data():
	var data = .get_data()
	data["type"] = "ResponseNode"
	data["text"] = $PanelContainer/Response.text
	
	return data
