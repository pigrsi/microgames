extends Sprite

export var popup_text = ""

var mouse_on = false
var connector_res = preload("res://DialogueTree/Connector.tscn")
var connector

func _on_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed and mouse_on:
			setup_connection(null)
		elif event.button_index == BUTTON_RIGHT and event.pressed and mouse_on:
			clear_connector()
			
func set_popup_text(text):
	popup_text = text
				
func setup_connection(to):
	clear_connector()
	connector = connector_res.instance()
	connector.set_from(self)
	connector.to = to
	if to:
		connector.connected = true
	get_tree().get_root().add_child(connector)

func clear_connector():
	if connector:
		connector.queue_free()
		
func get_connected_node():
	if not connector or not connector.to: return null
	return connector.to

func _on_mouse_entered():
	mouse_on = true
	if popup_text:
		$LineEdit.text = popup_text
		$LineEdit.visible = true

func _on_mouse_exited():
	mouse_on = false
	$LineEdit.visible = false
