extends Line2D

var connected = false
var from
var to

func set_from(from):
	self.from = from
	points[0] = from.global_position

func _process(delta):
	if connected: 
		if !from or !to:
			queue_free()
			return
		points[0] = from.global_position - global_position
		points[1] = to.global_position - global_position
	else:
		points[1] = get_local_mouse_position()
		$Area2D.position = points[1]
	
func _input(event):
	if connected: return
	if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT:
		queue_free()

func _on_area_entered(area):
	$Area2D.queue_free()
	connected = true
	to = area.get_parent()	
	points[1] = to.global_position - global_position
