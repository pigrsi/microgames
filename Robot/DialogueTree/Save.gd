extends Sprite

var mouse_on = false

func _on_Area2D_mouse_entered():
	mouse_on = true


func _on_Area2D_mouse_exited():
	mouse_on = false


func _on_Area2D_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and mouse_on and event.button_index == BUTTON_LEFT and event.pressed:
		get_tree().get_root().find_node("DialogueTree", false, false).save()
		
