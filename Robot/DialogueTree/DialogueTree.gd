extends Node2D

var nodeResources = [preload("res://DialogueTree/BotNode.tscn"),
 preload("res://DialogueTree/ResponseNode.tscn"),
 preload("res://DialogueTree/ConditionNode.tscn"),
 preload("res://DialogueTree/StartNode.tscn")]

var unique_name_value = -1
func get_unique_name():
	unique_name_value += 1
	return unique_name_value

func _init():
	OS.set_window_position(Vector2(110, 90))
	OS.set_window_size(Vector2(1600, 1000))
	OS.window_resizable = true

func _input(event):
	if event is InputEventKey and event.pressed:
		match event.scancode:
			KEY_F1: spawn_node(0)
			KEY_F2: spawn_node(1)
			KEY_F3: spawn_node(2)
			KEY_F4: spawn_node(3)
			
func load_nodes(json):
	if json.error != OK:
		print("load error")
		return
		
	for existing_node in $Nodes.get_children():
		existing_node.queue_free()
		
	for node in json.result:
		var new
		match node.type:
			"BotNode":
				new = spawn_node(0)
				new.set_values(node.text, node.emotion, node.id)
			"ResponseNode":
				new = spawn_node(1)
				new.set_values(node.text)
			"ConditionNode":
				new = spawn_node(2)
				new.set_values(node.text)
			"StartNode":
				new = spawn_node(3)
		new.position = Vector2(node.x, node.y)
		new.name = node.name
		
	for node in json.result:
		var real_node = $Nodes.get_node(node.name)
		for i in range(0, node.outputs.size()):
			if node.outputs[i]:
				var connect_to = $Nodes.get_node(node.outputs[i]).get_node("Input")
				real_node.get_node("Outputs").get_child(i).setup_connection(connect_to)
			
func spawn_node(index):
	var new = nodeResources[index].instance()
	new.position = get_local_mouse_position()
	new.name = str(get_unique_name())
	$Nodes.add_child(new)
	return new
				
func save():
	find_node("SaveDialog", true, false).popup()

func _on_SaveDialog_file_selected(path):
	var dialog = find_node("SaveDialog", true, false)
	
	var data = []
	for node in $Nodes.get_children():
		data.append(node.get_data())
	
	var file = File.new()
	file.open(dialog.get_current_path(), File.WRITE)
	file.store_string(JSON.print(data))
	file.close()
	
func load_():
	find_node("LoadDialog", true, false).popup()

func _on_LoadDialog_file_selected(path):
	var dialog = find_node("LoadDialog", true, false)

	var file = File.new()
	file.open(dialog.get_current_path(), File.READ)
	var content = JSON.parse(file.get_as_text())
	file.close()
	load_nodes(content)
