extends BaseNode

func set_values(text, emotion, id):
	var optionButton = $PanelContainer/VBoxContainer/Emotion/OptionButton
	
	for i in range(0, optionButton.get_item_count()):
		if emotion == optionButton.get_item_text(i):
			optionButton.selected = i
			break
	
	$PanelContainer/VBoxContainer/Text/TextBox.text = text
	$PanelContainer/VBoxContainer/ID/LineEdit.text = id
	
func get_condition_id():
	return $PanelContainer/VBoxContainer/ID/LineEdit.text

func get_data():
	var optionButton = $PanelContainer/VBoxContainer/Emotion/OptionButton
	
	var data = .get_data()
	data["type"] = "BotNode"
	data["text"] = $PanelContainer/VBoxContainer/Text/TextBox.text
	data["emotion"] = optionButton.get_item_text(optionButton.selected)
	data["id"] = $PanelContainer/VBoxContainer/ID/LineEdit.text
	
	return data
