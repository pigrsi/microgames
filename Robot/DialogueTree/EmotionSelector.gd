extends HBoxContainer

func _ready():
	var system = get_tree().get_root().find_node("System", true, false)
	if not system: return
	
	var emotions = system.get_list_of_emotions()
	for emotion in emotions:
		$OptionButton.add_item(emotion)
