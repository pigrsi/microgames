extends Node

var mouse_on = false
var mouse_on_delete = false
var dragging = false

func _input(event):
	if event is InputEventMouseButton:
		if mouse_on and event.button_index == BUTTON_LEFT:
			get_tree().set_input_as_handled()
			if event.pressed:
				dragging = true
			else:
				dragging = false
	elif dragging and event is InputEventMouseMotion:
		get_parent().position += event.relative


func _on_Area2D_mouse_entered():
	mouse_on = true

func _on_Area2D_mouse_exited():
	mouse_on = false

func delete():
	queue_free()
	
	
func _on_delete_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if mouse_on_delete and event.button_index == BUTTON_LEFT:
			get_parent().queue_free()

func _on_delete_mouse_entered():
	mouse_on_delete = true

func _on_delete_mouse_exited():
	mouse_on_delete = false
