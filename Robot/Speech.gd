extends NinePatchRect

var saying = ""
var index = 0
var letterDelay = 0.048

func _init():
	rect_scale = Vector2()

func say(text, delay=0):
	saying = text
	index = 0	
	$LetterTimer.wait_time += delay + 0.5
	$LetterTimer.start()
	show_box(delay)
	
func is_talking():
	return saying != ""

func _on_LetterTimer_timeout():
	if $LetterTimer.wait_time != letterDelay:
		$LetterTimer.wait_time = letterDelay
		$LetterTimer.start()
	
	index += 1
	if index <= saying.length():
		$SpeechBox.text = saying.substr(0, index)
	else:
		$LetterTimer.stop()
		saying = ""
		
		
func hide_box(delay=0):
	$ShowHideTween.stop_all()
	$ShowHideTween.interpolate_property(self, "rect_scale", null, Vector2(), 0.4, Tween.TRANS_QUAD, Tween.EASE_IN, delay)
	$ShowHideTween.start()
	
func show_box(delay=0):
	$SpeechBox.text = ""	
	$ShowHideTween.stop_all()
	$ShowHideTween.interpolate_property(self, "rect_scale", null, Vector2(1, 1), 0.4, Tween.TRANS_QUAD, Tween.EASE_IN, delay)
	$ShowHideTween.start()
