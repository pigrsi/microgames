extends Label

var duration = 1.75

func _ready():
	var finalColor = Color(self_modulate.r, self_modulate.g, self_modulate.b, 0)
	$Tween.interpolate_property(self, "self_modulate", null, finalColor, duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.interpolate_property(self, "rect_scale", null, Vector2(2.5, 2.5), duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.start()
	$DeathTimer.start()


func _on_DeathTimer_timeout():
	queue_free()
