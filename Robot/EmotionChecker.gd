extends Node2D

export var on = false
var emotion_index = 0
var robot
var emotion_list

func _ready():
	if on:
		visible = true
	else:
		queue_free()
		return
	
	robot = get_tree().get_root().find_node("Robot", true, false)
	emotion_list = robot.get_node("System").get_list_of_emotions()
	
	robot.select_emotion(emotion_list[0])
	$Label.text = "emotion: " + emotion_list[0]
	$Label2.text = "talking: " + str(robot.talking)

func _process(delta):
	if Input.is_action_just_pressed("ui_left"):
		emotion_index = (emotion_index - 1) % emotion_list.size()
		robot.select_emotion(emotion_list[emotion_index])
		$Label.text = "emotion: " + emotion_list[emotion_index]
	elif Input.is_action_just_pressed("ui_right"):
		emotion_index = (emotion_index + 1) % emotion_list.size()
		robot.select_emotion(emotion_list[emotion_index])
		$Label.text = "emotion: " + emotion_list[emotion_index]
	
	if Input.is_action_just_pressed("ui_down"):
		robot.talking = !robot.talking
		$Label2.text = "talking: " + str(robot.talking)
