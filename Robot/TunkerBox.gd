extends Node2D

var tree
var current_node
var state = ""
var selected_response_name

func feed_data(json):
	if json.error != OK:
		print("load error")
		return
		
	tree = json.result
	set_first_node()
	
	
func get_tree_node(nodeName):
	for node in tree:
		if node.name == nodeName:
			return node
	return null
	

func set_first_node():
	var startNodeName
	for node in tree:
		if node.type == "StartNode":
			set_current_node(get_tree_node(node.outputs[0]))
			
			
func set_current_node(node):
	current_node = node
	state = "initial"
	
			
func _process(delta):
	if not current_node: return
	if not $WaitTimer.is_stopped(): return
	match current_node.type:
		"BotNode":
			botNode()
		_:
			pass
			

func botNode():
	match (state):
		"initial":
			$Robot.select_emotion(current_node.emotion)
			$Robot.say(current_node.text, 1)
			state = "talking"
		"talking":
			if not $Robot/SpeechContainer.is_talking():
				state = "prompt_responses"
				wait(1)
		"prompt_responses":
			for i in range(0, 4):
				var output = current_node.outputs[i]
				if output != null:
					get_node("Robot/Responses/" + str(i)).show_box(get_tree_node(output).text)
			state = "await_response"
		"await_response":
			pass
		"response_received":
			var selected_response = get_tree_node(current_node.outputs[int(selected_response_name)])
			set_current_node(get_tree_node(selected_response.outputs[0]))
			
func wait(period):
	$WaitTimer.wait_time = period
	$WaitTimer.start()
		
func response_selected(nodeName):
	get_node("Robot/Responses/" + nodeName).spit_label()
	for node in $Robot/Responses.get_children():
		node.hide_box()
	$Robot.stop_talking(0.5)
	state = "response_received"
	selected_response_name = nodeName
	wait(1)
	
