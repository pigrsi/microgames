extends AnimatedSprite

var openSounds = []
var closeSounds = []

func _ready():
	openSounds = get_sound_files("res://sfx/open/")
	closeSounds = get_sound_files("res://sfx/close/")

func get_sound_files(path):
	var files = []
	var dir = Directory.new()
	if dir.open(path) == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if dir.current_is_dir():
				print("Found directory: " + file_name)
			else:
				print("Found file: " + file_name)
				if file_name.find("import") == -1:
					files.append(file_name)
			file_name = dir.get_next()
	else:
		print("An error occurred when trying to access the path.")
		
	return files

func _process(_delta):
	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		if frame != frames.get_frame_count("open") - 1:
			open()
	elif frame > 0:
			close()

func open():
	play("open")
	$Close.stop()
	if not $Open.playing:
		var sound = load("res://sfx/open/" + str(openSounds[randi() % openSounds.size()]))
		$Open.stream = sound
		$Open.play()
	
func close():
	play("open", true)
	$Open.stop()
	if not $Close.playing:
		var sound = load("res://sfx/close/" + str(closeSounds[randi() % closeSounds.size()]))
		$Close.stream = sound
		$Close.play()
